<?php
class Harimau extends Hewan{
    use Fight;

    public function __construct($name){
        $this->nama = $name;
        $this->jumlahKaki = 2;
        $this->keahlian = "lari cepat";
        $this->attackPower = 7;
        $this->defencePower = 8;
    }

    public function getInfoHewan(){
        echo "------------getInfo------------<br>";
        echo "nama :  $this->nama <br>";
        echo "darah :  $this->darah <br>";
        echo "jumlahKaki :  $this->jumlahKaki <br>";
        echo "keahlian :  $this->keahlian <br>";
        echo "attackPower :  $this->attackPower <br>";
        echo "defencePower :  $this->defencePower <br>";
        echo "------------end of getInfo------------<br><br>";
        
    }
}
?>
<?php

class Hewan{
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;

    public function atraksi(){
        echo $this->nama . ' sedang ' . $this->keahlian . '<br><br>';
    }
}

trait Fight{
    public $attackPower;
    public $defencePower;

    public function serang($hewan){
        
        echo $this->nama . " sedang menyerang " . $hewan->nama;
        echo "<br> darah $hewan->nama sebelum diserang : $hewan->darah";
        $hewan->darah -= $this->attackPower/$hewan->defencePower;

        echo ", setelah diserang jadi $hewan->darah <br><br>";
    }

    public function diserang($hewan){
        echo $this->nama . " sedang diserang " . $hewan->nama;
        echo "<br> darah $this->nama sebelum diserang : $this->darah";
        $this->darah -= $hewan->attackPower/$this->defencePower;

        echo ", setelah diserang jadi $this->darah <br><br>";
    }
}

class Elang extends Hewan{
    use Fight;

    public function __construct($name){
        $this->nama = $name;
        $this->jumlahKaki = 2;
        $this->keahlian = "terbang tinggi";
        $this->attackPower = 10;
        $this->defencePower=5;
    }

    public function getInfoHewan(){
        echo "------------getInfo------------<br>";
        echo "nama :  $this->nama <br>";
        echo "darah :  $this->darah <br>";
        echo "jumlahKaki :  $this->jumlahKaki <br>";
        echo "keahlian :  $this->keahlian <br>";
        echo "attackPower :  $this->attackPower <br>";
        echo "defencePower :  $this->defencePower <br>";
        echo "------------end of getInfo------------<br><br>";
        
    }
}

class Harimau extends Hewan{
    use Fight;

    public function __construct($name){
        $this->nama = $name;
        $this->jumlahKaki = 2;
        $this->keahlian = "lari cepat";
        $this->attackPower = 7;
        $this->defencePower = 8;
    }

    public function getInfoHewan(){
        echo "------------getInfo------------<br>";
        echo "nama :  $this->nama <br>";
        echo "darah :  $this->darah <br>";
        echo "jumlahKaki :  $this->jumlahKaki <br>";
        echo "keahlian :  $this->keahlian <br>";
        echo "attackPower :  $this->attackPower <br>";
        echo "defencePower :  $this->defencePower <br>";
        echo "------------end of getInfo------------<br><br>";
        
    }
}

$harimau_1 = new Harimau('harimau_1');
$elang_3 = new Elang('elang_3');

$harimau_1->serang($elang_3);
$harimau_1->diserang($elang_3);

$harimau_1->atraksi();



$elang_3->atraksi();

$harimau_1->serang($elang_3);

$harimau_1->getInfoHewan();
$elang_3->getInfoHewan();

?>
<?php

trait Fight{
    public $attackPower;
    public $defencePower;

    public function serang($hewan){
        
        echo $this->nama . " sedang menyerang " . $hewan->nama;
        echo "<br> darah $hewan->nama sebelum diserang : $hewan->darah";
        $hewan->darah -= $this->attackPower/$hewan->defencePower;

        echo ", setelah diserang jadi $hewan->darah <br><br>";
    }

    public function diserang($hewan){
        echo $this->nama . " sedang diserang " . $hewan->nama;
        echo "<br> darah $this->nama sebelum diserang : $this->darah";
        $this->darah -= $hewan->attackPower/$this->defencePower;

        echo ", setelah diserang jadi $this->darah <br><br>";
    }
}

?>
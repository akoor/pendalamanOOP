<?php
class Elang extends Hewan{
    use Fight;

    public function __construct($name){
        $this->nama = $name;
        $this->jumlahKaki = 2;
        $this->keahlian = "terbang tinggi";
        $this->attackPower = 10;
        $this->defencePower=5;
    }

    public function getInfoHewan(){
        echo "------------getInfo------------<br>";
        echo "nama :  $this->nama <br>";
        echo "darah :  $this->darah <br>";
        echo "jumlahKaki :  $this->jumlahKaki <br>";
        echo "keahlian :  $this->keahlian <br>";
        echo "attackPower :  $this->attackPower <br>";
        echo "defencePower :  $this->defencePower <br>";
        echo "------------end of getInfo------------<br><br>";
        
    }
}

?>